var $current;

$(window).load( function() {
	$('.projects').click( function() {
		if($current == null) {
			$(this).find('.hidden').fadeTo('fast',1);
			$(this).animate( {height:$(this).find('.description').height()} );
			$current = $(this);
		} else if($current.is($(this))) {
			$current.find('.hidden').fadeTo('fast',0);
			$current.animate({height:'96px'});
			$current=null;
		} else {
			$current.find('.hidden').fadeTo('fast',0);
			$current.animate( {height:'96px'} );
			$(this).find('.hidden').fadeTo('fast',1);
			$(this).animate({height:$(this).find('.description').height()});
			$current=$(this);
		}
	})
});